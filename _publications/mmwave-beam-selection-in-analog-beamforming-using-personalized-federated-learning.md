---
title: mmWave Beam Selection in Analog Beamforming Using Personalized Federated Learning
author: M. Isaksson, F. Vannella, D. Sandberg, R. Cöster
in: IEEE Future Networks World Forum 2023 (Best Paper Award)
date: 2023-10-03
tags:
    - 5G
    - federated learning
    - machine learning
    - beam selection
    - distributed learning
    - federated learning
    - privacy
online: https://arxiv.org/abs/2310.00406
abstract: |
    Using analog beamforming in mmWave frequency bands we can focus the energy towards a receiver to achieve high throughput. However, this requires the network to quickly find the best downlink beam configuration in the face of non-IID data. We propose a personalized Federated Learning (FL) method to address this challenge, where we learn a mapping between uplink Sub-6GHz channel estimates and the best downlink beam in heterogeneous scenarios with non-IID characteristics. We also devise FedLion, a FL implementation of the Lion optimization algorithm. Our approach reduces the signalling overhead and provides superior performance, up to 33.6 % higher accuracy than a single FL model and 6 % higher than a local model.
bibtex: |
    @misc{isaksson2023mmwave,
      title={mmWave Beam Selection in Analog Beamforming Using Personalized Federated Learning}, 
      author={Martin Isaksson and Filippo Vannella and David Sandberg and Rickard Cöster},
      year={2023},
      eprint={2310.00406},
      archivePrefix={arXiv},
    }
---

{{ page.abstract }}

<iframe width="560" height="315" src="https://www.youtube.com/embed/NrgvFSIg2so?si=WrL7zerNVRju51Is" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
