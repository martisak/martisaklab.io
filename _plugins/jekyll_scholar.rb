# Contributed by @mfenner
# See https://github.com/inukshuk/jekyll-scholar/issues/30

require 'jekyll/scholar'
require 'uri'

module MarkdownFilter
  class MarkdownHTMLLink < BibTeX::Filter
    def apply(value)
      value.to_s.gsub(URI.regexp(['http','https','ftp'])) { |c| "<a href=\"#{$&}\" rel=\"noopener noreferrer\" target=\"_blank\">#{$&}</a>" }
      # value.to_s.gsub(URI.regexp(['http','https','ftp'])) { |c| "[#{$&}](#{$&})" }
    end
  end
end