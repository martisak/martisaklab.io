require 'git'

module Jekyll

  class PagePathGenerator < Generator
    safe true
    ## See post.dir and post.base for directory information.
    def generate(site)

      g = Git.open(".")
      site.collections["posts"].docs.each do |post|
        post.data['revisions'] = g.log.object(post.relative_path).map do |commit|
          {"date" => commit.date,
           "author" => commit.author.name,
           "message" => commit.message,
           "hash" => commit.sha}
        end
      end

      site.collections["publications"].docs.each do |post|
        post.data['revisions'] = g.log.object(post.relative_path).map do |commit|
          {"date" => commit.date,
           "author" => commit.author.name,
           "message" => commit.message,
           "hash" => commit.sha}
        end
      end

    end
  end
end