var paths = {};
paths.assetsDir        = '_assets/';      // The files Gulp will handle.
paths.jekyllDir        = '';              // The files Jekyll will handle.
//paths.jekyllAssetsDir  = 'assets/';       // The asset files Jekyll will handle.
paths.siteDir          = 'public/';        // The resulting static site.
paths.siteAssetsDir    = 'public/assets/'; // The resulting static site's assets.

paths.siteFolder = 'public';
paths.stylesFolder = 'css';
paths.imageFolder  = 'images';
paths.fontFolder   = 'fonts';
paths.scriptFolder   = 'js';

// Asset files locations.
paths.sassFiles   = paths.assetsDir + paths.stylesFolder;
paths.scriptFiles   = paths.assetsDir + paths.scriptFolder;
paths.siteScriptFiles   = paths.siteAssetsDir + paths.scriptFolder;

//paths.jekyllCssFiles   = paths.jekyllAssetsDir + paths.stylesFolder;
paths.siteCssFiles   = paths.siteAssetsDir + paths.stylesFolder;

paths.fontFiles   = paths.assetsDir + paths.fontFolder;
//paths.jekyllFontFiles  = paths.jekyllAssetsDir + paths.fontFolder;
paths.siteFontFiles  = paths.siteAssetsDir + paths.fontFolder;

paths.imageFiles  = paths.assetsDir + paths.imageFolder;
//paths.jekyllImageFiles = paths.jekyllAssetsDir + paths.imageFolder;
paths.siteImageFiles = paths.siteAssetsDir + paths.imageFolder;

paths.imagePattern    = '/**/*.+(jpg|jpeg|png|svg|gif|webp|tif|webm)';
//paths.jekyllImageFilesGlob = paths.jekyllImageFiles + paths.imagePattern;
paths.imageFilesGlob = paths.imageFiles + paths.imagePattern;

paths.rootImages = paths.jekyllDir;
paths.rootImagesSite = paths.siteFolder;
paths.rootImagePattern = '*.+(jpg|jpeg|png|svg|gif|webp|tif|webm)';
paths.rootImageFilesGlob = paths.rootImages + paths.rootImagePattern;
paths.siteImageFilesGlob = paths.rootImagesSite + "/";

paths.imagesImageFilesGlob = paths.imageFolder + "/" + paths.rootImagePattern;
paths.siteImageImageFilesGlob = paths.siteFolder + "/" + paths.imageFolder + "/";

const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const cssnano = require('cssnano');
var clean = require('gulp-clean');
const gulp = require('gulp');
const gutil = require('gulp-util');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const run = require('gulp-run');
const runSequence = require('run-sequence');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify-es').default; // For es6 support
const htmlmin = require('gulp-htmlmin');
var log = require('fancy-log');
var argv = require('yargs').argv;

const terser = require('gulp-terser');

const rev = require('gulp-rev');
const revRewrite = require('gulp-rev-rewrite');
var revcss = require('gulp-rev-css-url');
var revdel = require('gulp-rev-delete-original');

path = require('path');

function buildScripts() {
    return gulp.src(paths.scriptFiles + '/*.*')
        .pipe(concat('scripts.js'))
        .pipe(terser())
        .pipe(gulp.dest(paths.siteScriptFiles))
        .pipe(browserSync.stream())
        .on('error', log.error);
}

function copyJquery() {
    return gulp.src("node_modules/jquery/dist/jquery.min.*")
        .pipe(gulp.dest(paths.siteScriptFiles))
        .pipe(browserSync.stream())
        .on('error', log.error);
}

function copyAnchorJS() {
  return gulp.src("node_modules/anchor-js/anchor.min.*")
      .pipe(gulp.dest(paths.siteScriptFiles))
      .pipe(browserSync.stream())
      .on('error', log.error);
}

function copyMeramaidJS() {
  return gulp.src("node_modules/mermaid/dist/mermaid.min.js")
      .pipe(gulp.dest(paths.siteScriptFiles))
      .pipe(browserSync.stream())
      .on('error', log.error);
}

function sassTask() {
return gulp.src(paths.sassFiles + '/*.scss')
    .pipe(sass({errLogToConsole: true, outputStyle: 'compressed'}))
    .pipe(
        postcss(
            [
            autoprefixer({ overrideBrowserslist: ['last 2 versions']}),
            cssnano({discardComments: {removeAll: true}})
            ]))
    //.pipe(gulp.dest(paths.jekyllCssFiles))
    .pipe(concat('main.css'))
    .pipe(gulp.dest(paths.siteCssFiles))
    .pipe(browserSync.stream())
    .on('error', gutil.log);
};

exports.styles = sassTask;

// Optimize and copy image files
function buildImages() {
  return gulp.src(paths.imageFilesGlob)
    .pipe(imagemin({
        optimizationLevel: 4,
        progressive: true,
        interlaced: false,
        use: [pngquant()]
    }))
    //.pipe(gulp.dest(paths.jekyllImageFiles))
    .pipe(gulp.dest(paths.siteImageFiles))
    .pipe(browserSync.stream());
};

// Optimize and copy image files in root
function buildRootImages() {
  log("Glob:" + paths.rootImageFilesGlob);
  log("Glob:" + paths.siteImageFilesGlob);
  return gulp.src(paths.rootImageFilesGlob)
    .pipe(imagemin({
        optimizationLevel: 4,
        progressive: true,
        interlaced: false,
        use: [pngquant()]
    }))
    .pipe(gulp.dest(paths.siteImageFilesGlob))
    .pipe(browserSync.stream());
};


// Optimize and copy image files in root
function buildImageImages() {
  log(paths.siteImageImageFilesGlob)
  return gulp.src(paths.imagesImageFilesGlob)
    .pipe(imagemin({
        optimizationLevel: 4,
        progressive: true,
        interlaced: false,
        use: [pngquant()]
    }))
    .pipe(gulp.dest(paths.siteImageImageFilesGlob))
    .pipe(browserSync.stream());
};

function buildJekyll() {
  var shellCommand = 'bundle exec jekyll build --config _config.yml -d public';

  if (argv.drafts) {
    shellCommand += " --profile --drafts --trace"
  }

  log('Jekyll command' + shellCommand);
  return gulp.src('.', {allowEmpty: true})
    .pipe(run(shellCommand))
    .on('error', gutil.log);
};

// Delete the entire _site directory
function cleanJekyll() {
    return gulp.src([paths.siteFolder], {read: false})
         .pipe(clean());
};

function minifyHtml() {
    return gulp.src([
        path.join(paths.siteFolder, '*.html'),
        path.join(paths.siteFolder, '*/*/*.html'),
        path.join(paths.siteFolder, '*/*/*/*.html'),
        path.join(paths.siteFolder, '*/*/*/*/*.html')
    ])
        .pipe(htmlmin(
            {
                collapseWhitespace: true, minifyCSS: true, 
                minifyURLs: true, minifyJS: true, removeComments: true
            }
        ))
        .pipe(gulp.dest(paths.siteFolder))
        .pipe(browserSync.stream({once: true}))
        .on('error', gutil.log);
};

 function buildFonts() {
  return gulp.src(paths.fontFiles + '/*.*')
    //.pipe(rename(function(path) {path.dirname = '';}))
    //.pipe(gulp.dest(paths.jekyllFontFiles))
    .pipe(gulp.dest(paths.siteFontFiles))
    .pipe(browserSync.stream())
    .on('error', gutil.log);
}

function jekyllWatch(callback) {
    browserSync.reload();
    callback();
}

// // Step 1
function revision() {
  return gulp.src('public/**/*.{css,js,json}')
    .pipe(rev())
    .pipe(revcss())
    .pipe(revdel())
    .pipe(gulp.dest('public'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('public'));
}

// // Step 2
function rewrite() {
  const manifest = gulp.src('public/rev-manifest.json');

  return gulp.src('public/**/*.html')
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest('public'));
}

exports.sass = sassTask;

exports.build = gulp.series( 
        buildJekyll,
        sassTask,
        minifyHtml,
        buildRootImages, 
        buildImageImages,
        buildImages,
        buildFonts,
        buildScripts,
        copyJquery,
        copyAnchorJS,
        copyMeramaidJS,
        revision, 
        rewrite
    );

function serveSite() {
    browserSync.init({
    server: paths.siteDir,
    ghostMode: false, // Toggle to mirror clicks, reloads etc (performance)
    logFileChanges: true,
    logLevel: 'debug',
    open: true       // Toggle to auto-open page when starting
  });

  gulp.watch(['_config.yml'], gulp.series(exports.build, jekyllWatch));
  // Watch .scss files and pipe changes to browserSync
  gulp.watch('_assets/css/**/*.scss', gulp.series( 
        buildJekyll,
        sassTask,
        minifyHtml,
        buildRootImages, 
        buildImageImages,
        buildImages,
        buildFonts,
        buildScripts,
        copyJquery,
        copyAnchorJS,
        copyMeramaidJS,
        revision, 
        rewrite
    ));

  // Watch image files and pipe changes to browserSync
  gulp.watch('_assets/images/**/*', buildImages);
  // Watch posts
  gulp.watch('_posts/**/*.+(md|markdown|MD)', gulp.series(exports.build, jekyllWatch));
  // Watch drafts if --drafts flag was passed
  if (argv.drafts) {
    gulp.watch('_drafts/*.+(md|markdown|MD)', gulp.series(exports.build, jekyllWatch));
  }
  // Watch html and markdown files
  gulp.watch(['**/*.+(html|md|markdown|MD)', '!public/**/*.*'], gulp.series(exports.build, jekyllWatch));
  // Watch RSS feed
  //gulp.watch('feed.xml', gulp.series(exports.build, jekyllWatch));
  // Watch data files
  gulp.watch('_data/**.*+(yml|yaml|csv|json)', gulp.series(exports.build, jekyllWatch));

}

exports.serve = gulp.series(exports.build, serveSite)
exports.clean = cleanJekyll;

exports.default = exports.build