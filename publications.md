---
layout: collection
permalink: /publications/
title: Publications
description: List of my scientific publications
---

Please find the compilation of my academic publications below, in reverse chronological order. My work can be found in <i class="ai ai-google-scholar-square"></i>&nbsp;[Google Scholar](http://scholar.google.com/citations?user=B-ZqdUkAAAAJ&hl=en), <i class="ai ai-semantic-scholar-square"></i>&nbsp;[Semantic Scholar](https://www.semanticscholar.org/author/Martin-Isaksson/15929031) and <i class="ai ai-dblp-square"></i>&nbsp;[dblp](https://dblp.org/pid/220/4387.html), with <i class="ai ai-orcid-square"></i>&nbsp;ORCID&nbsp;[0000-0001-9972-0179](https://orcid.org/0000-0001-9972-0179).

{% assign sorted = site.publications | sort: 'date' | reverse %}

<div class="entries-{{ page.entries_layout | default: 'list' }}">
  {% for entry in sorted %}
    {% include publication_entry.html %}
  {% endfor %}
</div>
