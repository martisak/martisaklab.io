---
layout: post
title: Publishing IEEE pre-prints
subtitle: Beating reviewer number 2
image:
  path:  /images/blog.jpg
hidden: false
published: true
showrevisions: true
categories:
    - academia
    - latex
tags:
    - latex
toc: true
---

**If you have submitted or plan to submit your paper to an [IEEE](https://www.ieee.org/) journal or conference, you might want to consider posting your pre-print in [arXiv.org](arXiv.org) or [TechRxiv.org](TechRxiv.org), on your employer’s website or institutional repository and on your personal website. IEEE does not consider this to be a form of prior publication, see [IEEE Post-Publication Policies](https://journals.ieeeauthorcenter.ieee.org/become-an-ieee-journal-author/publishing-ethics/guidelines-and-policies/post-publication-policies/). But what are the practical steps to do so? In this post we cover the mandatory steps you have to take in order to publish an IEEE article as a pre-print.**

<!--more-->

## Why is this important?

In the fast-paced world of scientific research, the dissemination of knowledge is crucial for advancing our understanding of the world around us. Traditionally, the process of scientific publishing has been slow and rigid, often resulting in significant delays between the completion of research and its availability to the wider community. However, a groundbreaking development has been reshaping the landscape of academic publishing – the rise of pre-prints.  Pre-prints are early versions of research papers made publicly available before formal peer review, offering researchers the opportunity to share their findings rapidly and to get early feedback. It has been shown that papers with pre-prints have a citation edge {% cite conroy_preprints_2019 xie2021is %}, and that this effect is clear, immediate and long-lasting.

## Consider this first

Before you continue, please consult the author information for the conference or journal that you will or have submitted to. After making sure that you are permitted to publish a pre-print, then you have to follow the rules with regards to the information that needs to be put on the first page, see [IEEE Post-Publication Policies](https://journals.ieeeauthorcenter.ieee.org/become-an-ieee-journal-author/publishing-ethics/guidelines-and-policies/post-publication-policies/) {% cite noauthor_post-publication_nodate %}. But how exactly do you do that? As usual in LaTeX, there are about a million ways, here is one that I like. Make sure you compile at least twice (which you already probably already do).

See [Putting IEEE copyright on a document title page](https://tex.stackexchange.com/questions/567084/putting-ieee-copyright-on-a-document-title-page), [How to add copyright notice (in a box with borders) at bottom of first page?
](https://tex.stackexchange.com/questions/55813/how-to-add-copyright-notice-in-a-box-with-borders-at-bottom-of-first-page) and 
[IEEEtran conference with \maketitle - copyright notice (in a box with borders) at bottom of first page?](https://tex.stackexchange.com/questions/154503/ieeetran-conference-with-maketitle-copyright-notice-in-a-box-with-borders-a) for more information on the subject.

{% include figure.html url="/assets/images/ieee-copyright-1-0.png" description="Example." %}

## After submitting a paper 

We have to add the text

> This work has been submitted to the IEEE for possible publication. Copyright may be transferred without notice, after which this version may no longer be accessible.

{% highlight tex linenos %}
\usepackage{tikz}

\newcommand\submittedtext{{ '{%' }}
  \footnotesize This work has been submitted to the IEEE for possible publication. Copyright may be transferred without notice, after which this version may no longer be accessible.}

\newcommand\submittednotice{{ '{%' }}
\begin{tikzpicture}[remember picture,overlay]
\node[anchor=south,yshift=10pt] at (current page.south) {\fbox{\parbox{\dimexpr0.65\textwidth-\fboxsep-\fboxrule\relax}{\submittedtext}}};
\end{tikzpicture}%
}
{% endhighlight %}

## After the paper is accepted and published

We have to add the text

> © 20XX IEEE.  Personal use of this material is permitted.  Permission from IEEE must be obtained for all other uses, in any current or future media, including reprinting/republishing this material for advertising or promotional purposes, creating new collective works, for resale or redistribution to servers or lists, or reuse of any copyrighted component of this work in other works.

{% highlight tex linenos %}
\usepackage{tikz}

\newcommand\copyrighttext{{ '{%' }}
  \footnotesize \textcopyright \the\year{} IEEE. Personal use of this material is permitted. Permission from IEEE must be obtained for all other uses, including reprinting/republishing this material for advertising or promotional purposes, collecting new collected works for resale or redistribution to servers or lists, or reuse of any copyrighted component of this work in other works.}

\newcommand\copyrightnotice{{ '{%' }}
\begin{tikzpicture}[remember picture,overlay]
\node[anchor=south,yshift=10pt] at (current page.south) {\fbox{\parbox{\dimexpr0.75\textwidth-\fboxsep-\fboxrule\relax}{\copyrighttext}}};
\end{tikzpicture}%
}
{% endhighlight %}


## Finally

On the first page (for example after `\maketitle`, add `\copyrightnotice` or `\submittednotice` as needed.

In case we want the box to be red and be 2pt wide we can of course do that.

{% highlight tex linenos %}
\renewcommand\fbox{\fcolorbox{red}{white}}
\setlength{\fboxrule}{2pt} % Set fbox rule width to 2pt
{% endhighlight %}
