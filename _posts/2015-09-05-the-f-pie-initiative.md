---
title:  "The F Pie Initiative"
date:   2015-09-05 21:00:00
categories: 
    - visualization 
tags:
    - pie-charts
    - rant
image: 
  path: /images/blog-4.jpg
---

**Sometimes, when I come across a Powerpoint presentation with multiple multi-slice pie charts on a single slide my head hurts and I have to go air-poop for half-an-hour. Now I have decided to take action!**

<!--more-->

## Introducing The F Pie Initiative

I am proud to present an initiative to end bad pie charts. Not all pie charts, just the bad ones.. which is most of them. Members in this elite club pledge to only make beautiful, easily interpretable graphs.

<img src="/images/fpie.png" class="center-block" />

## So what's wrong with pie charts?

{% quote Tufte:1986:VDQ:33404 %}
Tables are clearly the best way to show exact numerical values, although the entries can also be arranged in semi-graphical form. Tables are preferable to graphics for many small data sets. <b>A table is nearly always better than a dumb pie chart; the only worse design than a pie chart is several of them</b>, for then the viewer is asked to compare quantities located in spatial disarray both within and between pies, as in this heavily encoded example from an atlas. Given their low data-density and failure to order numbers along a visual dimension, pie charts should never be used.
{% endquote %}

{% quote Few2007 %}
Here sits the friendly pie chart. Its slices are upturned into an inviting smile. Its simple charm is beloved by all but a few, welcomed almost everywhere; familiar and rarely threatening. Of all the graphs that play major roles in the lexicon of quantitative communication, however, the pie chart is by far the least effective. <b>Its colorful voice is often heard, but rarely understood. It mumbles when it talks.</b>
{% endquote %}

## Is there anything good about pie charts?

So can the pie chart be used for anything? Sure, pie charts can be effective in showing the “part-to-whole relationship" {% cite Few2007 %}, but the problem is that this is not how they are used.

## Get your membership card today!

Make the world a better place, become a member today!

<img src="/images/card.png" style="width:60%" class="center-block"/>
