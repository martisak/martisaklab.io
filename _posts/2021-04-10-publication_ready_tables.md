---
title:  Create publication ready tables with Pandas
categories: reproducibility academia visualization
layout: "post"
tags:
  - visualization
  - tables
  - example
image:
  path: /images/blog-2.jpg
description: Learn how to make your tables stand out with Pandas.
---

**Tables in scientific papers often look less than professional, and
sometimes this can even get in the way of understanding the message. In this
blog post we will use `pandas` to automate making
publication ready LaTeX tables that look great.**

<!--more-->

## Introduction

Tufte argues that we should strive to have a high data to ink-ratio
{% cite Tufte:1986:VDQ:33404 %} which means that we should strive to remove redundant graphical element that do not contribute to conveying our message. This applies to tables as well.

For typesetting tables in my scientific papers I use
LaTeX with the `booktabs` {% cite Fear2020 %} package. Using `booktabs` goes a long way towards
making beautiful tables with a high data to ink-ratio, but it's a manual process.

<!-- 

http://www.inf.ethz.ch/personal/markusp/teaching/guides/guide-tables.pdf
https://tex.stackexchange.com/questions/112343/beautiful-table-samples
https://tex.stackexchange.com/questions/291786/how-to-print-tabular-confidence-intervals-as-x-y-with-siunitx
https://gist.github.com/flutefreak7/50ffd291eaa348ead35c9794587006df
https://twitter.com/EdwardTufte/status/362274598819078144
-->

{% include figure.html url="/images/trimmed-table.png" description="Example figure produced with this method." colclass="col-md-12" %}

In this blog post we will explore using `pandas` {% cite reback2020pandas mckinney-proc-scipy-2010 %} and `booktabs` for removing some
unwanted ink from our tables and building a pipeline for generating and including the tables into our LaTeX papers.

## Using `pandas` to make a table

The first thing we need to do is to make a table from a dataset. We'll look at
the Iris dataset&nbsp;{% cite fisher1936use %} from the `seaborn` {% cite Waskom2021 %} Python library. 

We could simply use the `pandas` function [`to_latex()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_latex.html) to save a file containing the table in LaTeX format. `pandas` requires `booktabs`, but we can make this table even better with some simple tweaks.

{% include figure.html url="/images/trimmed-table_orig.png" description="Example table using the Iris dataset from the `seaborn` library." colclass="col-md-12" %}

First we want to specify the table column format and round the numbers to two decimals. Secondly, we want to highlight the maximum number in a column by making the numbers bold. And lastly, we want to make each column header bold.

Specifying the table format is easy using the `siunitx` package {% cite Wright2009 %}. We set each of the number columns to `S[table-format = 2.2]`.

Making the maximum value in each column bold requires a bit more work. {% cite Kalinke2020 %} wrote an inspiring post that we make use of here. Since we are using `siunitx` to specify the column format we use `\bfseries` to make numbers bold and allow `siunitx` to detect this by loading the package with `\usepackage[round-mode=places,detect-weight=true,detect-inline-weight=math]{siunitx}`. 

Column header titles should be bold and in title case, so we directly modify `df.columns` to achieve this.

Since we added LaTeX tags to our table we must set `escape` to `False` in the `to_latex` call.

{% highlight python linenos %}
{% include tables/table.py %}
{% endhighlight %}

At the end we are using the `pandas` function [`to_latex()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_latex.html) to generate the LaTeX code and write the result to a file containing the `tabular` environment. For this example, we have used `seaborn==0.11.1` and `pandas==1.2.3`.

Now we are ready to include the generated file into a LaTeX document.

{% highlight latex linenos %}
{% include tables/table.tex %}
{% endhighlight %}

We can compile this as a standalone figure into PNG and PDF by running
`pdflatex -shell-escape figure.tex`.

### Automation

To automate this build, we can use the following `Makefile`. The input files 
are `table.py` and `table.tex`, both of which are listed above.

{% highlight makefile linenos %}
{% include tables/Makefile %}
{% endhighlight %}

We want to create the file `table.png`. To do this we start with running
Python to generate the `.tbl` file that we then include in `table.tex`.
Compiling `table.tex` renders the table and saves it as a `.png`. We get
a `.pdf` for free when using `pdflatex`.

## Related Work

{% cite Kalinke2020 %} was the inspiration for this post. The method used herein to make numbers bold included code for formatting the numbers. In this work we use `siunitx` instead to do the formatting.

In [R](https://www.r-project.org/) we can use packages [`xtable`](https://cran.r-project.org/web/packages/xtable/index.html) or [`kableExtra`](https://haozhu233.github.io/kableExtra/) to achieve similar results. In particular, `kableExtra` is very capable and the documentation {% cite Zhu2020 %} has many interesting examples.

The entire library of work by Edward Tufte is hugely inspirational to us.
{% cite Tufte:1986:VDQ:33404 %} tells us not to put too much ink on the paper.

## Conclusion

We have looked at how to make tables generated by `pandas` to look more
professional by using `siunitx` and some tweaks. The `Makefile` we created
should go into the `tables` directory of your manuscript so that you can use
`make -C tables all` as a dependency to your normal `make report` target.

Easily digested tables makes it easier to understand the message we are trying 
to convey. In fact there is some
evidence&#160;{% cite Huang2018 %} that the visual appearance of a paper is
important and that improving the paper gestalt reduces risk of getting a paper
rejected.